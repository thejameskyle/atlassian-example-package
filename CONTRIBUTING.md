# Contributing

| Command          | Description             |
| ---------------- | ----------------------- |
| `yarn dev`       | Runs Jest in watch mode |
| `yarn test`      | Runs Jest once          |
| `yarn lint`      | Runs ESLint & Prettier  |
| `yarn typecheck` | Runs Flow               |
| `yarn build`     | Runs Babel              |
