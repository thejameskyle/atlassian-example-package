# Example Atlassian Package

> This repository sets up a default set of tools to create new packages

### Usage

```sh
git clone git@bitbucket.org:thejameskyle/atlassian-example-package.git my-package
cd my-package
rm -rf .git
git init
```

* Update `package.json#private`
* Update `package.json#name`
* Update `package.json#description`
* Update `package.json#repository`
* If your package isn't going to be open source:
  * Remove `LICENSE`
  * Update `package.json#license` to `"UNLICENSED"`
* Read `CONTRIBUTING.md`
* Remove this content from the `README.md`
