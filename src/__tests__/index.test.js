// @flow
import greeting from '../';

test('greeting', () => {
  expect(greeting('World')).toBe('Hello, World.');
});
