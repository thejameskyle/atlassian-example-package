// @flow
export default function greeting(target: string) {
  return `Hello, ${target}.`;
}
